#!/bin/bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip

docker run --rm -it \
	-e DISPLAY=$ip:0 \
	--env="QT_X11_NO_MITSHM=1" \
	-p 8888:8888 \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="/Users/patrickle/Desktop/University_Work/Git/summerstudio2020:/root/studio/" \
	--privileged \
	benthepleb/summerstudio2020:latest \
	bash
