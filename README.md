# Summer Studio Repository

## Docker Images Information
If you use the ```latest``` tag for the Docker image - it utilises the image that Tensorflow as a base image. For the ```stable``` tag for the Docker image - it utilises the image that Intel has provided as the base image. The ```quiz``` tag for the Docker image was a precursor to get things up and running.

We will be using the ```latest``` tag for our classroom coding going forward as it utilises TensorFlow version 2.1. The ```stable``` tag utilises TensorFlow version 2.0.

Refer to the ```docker``` folder for the Dockerfile that was used to build the images.

To pull the latest tag 

```bash
docker pull benthepleb/summerstudio2020:latest
```

## Useful bash script
If you try run the ```notebook.sh``` it should run the Jupyter Lab

```bash
bash notebook.sh
```

If you try run the ```run.sh``` it will allow you to run the Docker container's bash terminal.

```bash
bash run.sh
```

## Tip and Tricks for running Docker Containers
If you refer to the file ```run.sh```, there are specific arguments that I have used to get certain functionalities of the Docker Container working.

```bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip
```

This command above allows us to do X11 Forwarding so we are able to see GUIs that pop up in the Docker Container. It does it by grabbing the ip address of your computer and providing network access to xhost.

```bash
-p 8888:8888
```

This command above allows us to ensure that the port 8888 is open for us to establish a connection between jupyter lab in the Docker Container and your computer

```bash
--volume="<your_own_volume>:<directory_in_docker>"
```

This command above is a template on how we mount folders into our Docker Container

We also need to apply these arguments to get GUIs working in the Docker Container.
```bash
-e DISPLAY=$ip:0 \
--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw \"
--env="QT_X11_NO_MITSHM=1" \
--privileged
```

To run Jupyter Lab, we require it to be done at launching the Docker Container.
```bash
jupyter lab --ip 0.0.0.0 --port 8888 --allow-root --no-browser
```

## Running an IDE with Docker
Refer to this website on how to setup python remote interpreter for PyCharm. https://www.jetbrains.com/help/pycharm/configuring-remote-interpreters-via-ssh.html 
Make sure you have installed the PyCharm Professional Edition that is available on the Education License.
